(add-to-list 'load-path "~/.emacs.d/elpa/htmlize-20180923.1829")
(require 'org)
(require 'htmlize)
(require 'font-lock)
(require 'ox-html)

(setq org-html-htmlize-output-type 'css)

(require 'ox-latex)
(setq org-latex-listings 'listings) 


;; Fontify the source code (require 'htmlize)
(setq org-src-fontify-natively t)

(setq org-confirm-babel-evaluate nil)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t)
   (emacs-lisp . t)
   (R . t)
   (shell . t)
   (latex . t)
   (gnuplot . t)
   (fortran . t)))

(setq org-publish-project-alist
       '(("html"
          :base-directory "."
          :base-extension "org"
          :recursive t
          :publishing-directory "./public"
          :exclude "README.org" 
          :publishing-function org-html-publish-to-html)
         ("imgs"
          :base-directory "."
          :base-extension "jpg\\|gif\\|png"
          :publishing-directory "./public"
          :publishing-function org-publish-attachment
          :recursive t)
         ("tikzgallery" :components ("html" "imgs"))))

(provide 'publish)
